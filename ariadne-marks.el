;;; ariadne-marks.el --- Keep a list of buffer locations -*- lexical-binding: t -*-
;; [[file:~/p/elisp/mw/ariadne-marks/ariadne-marks.org::*Main%20Program][Main\ Program:1]]
;; [[file:~/p/elisp/mw/ariadne-marks/ariadne-marks.org::*Prologue][prologue]]

;; THIS FILE HAS BEEN GENERATED.

;; Copyright (C) 2010-2017 Marco Wahl

;; Author: Marco Wahl <marcowahlsoft@gmail.com>
;; Maintainer: Marco Wahl <marcowahlsoft@gmail.com>
;; Created: 201702242151
;; Keywords: navigation
;; Homepage: http://marcowahl.github.io
;; Version: 0.0.0
;; Keywords: convenience
;; Package-Requires: ((emacs "24"))

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; prologue ends here
;; [[file:~/p/elisp/mw/ariadne-marks/ariadne-marks.org::*Commentary][commentary]]

;;; Commentary:


;; THIS FILE HAS BEEN GENERATED.

;; ariadne-marks.el provides an additional list of buffer locations.

;; These buffer locations are thought
;; - to be easy to use.
;; - not to pollute the universe of the Emacs standard marks.
;; - often save just _one_ location to get back to.

;; There are a few functions to control the ariadne-marks.

;; You might want to think about the ariadne as a thread through the
;; buffer labyrinth.  Unlike in the saga of Ariadne you can set
;; ariadne-marks for easy revisit with =ariadne-marks-set-mark=.  You can
;; always go to the ariadne-mark at the end with =ariadne-marks-goto-end=
;; i.e. the ariadne-mark you set last (except you deleted that mark).
;; You can move backwards to the next mark in the ariadne-thread with
;; =ariadne-marks-backward=.  When you are on an ariadne-mark you can remove
;; that mark with =ariadne-marks-unset=.  Further you can unset all marks at
;; once with =ariadne-marks-unset-all=.

;; If a command can't do something reasonably it just does nothing,
;; e.g. unset a mark where there is none.

;; The locations are independent of the standard marks.

;; One user uses key-chords a6-a9 for the commands =ariadne-marks-goto-end=
;; (a6), =ariadne-marks-backward= (a7), =ariadne-marks-set-mark= (a8),
;; =ariadne-marks-unset= (a9) and finally =ariadne-marks-unset-all= (a0) if the
;; thread becomes too confusing e.g. if ariadne-marks point to nirvana.

;; One user often uses ariadne to maintain just one location to get back
;; to easily, as the historical Ariadne did, for what it's worth.

;; See also https://www.emacswiki.org/emacs/spin-markers.el for another
;; lightweight marker environment.
;; commentary ends here
;; [[file:~/p/elisp/mw/ariadne-marks/ariadne-marks.org::*Code%20gist][ariadne-marks-code]]

;;; Code:

(defvar *ariadne-marks-thread* nil)
(defvar *ariadne-marks-current* nil
  "Always points to the position last visited or set.")

(defun ariadne-marks-set-mark ()
  "Add (point) to ariadne points."
  (interactive)
  (if (and *ariadne-marks-thread*
               (equal (cons (current-buffer) (point))
                      (car *ariadne-marks-thread*)))
      (message "Ariadne-mark set (mark has been there already)")
    (push (cons (current-buffer) (point)) *ariadne-marks-thread*)
    (setq *ariadne-marks-current* *ariadne-marks-thread*)
    (message "Ariadne-mark set")))

(defun ariadne-marks-goto-end ()
  "Goto the last point in the ariadne list."
  (interactive)
  (if *ariadne-marks-thread*
      (progn
        (ariadne-marks--goto-buffer-location (car *ariadne-marks-thread*))
        (setq *ariadne-marks-current* *ariadne-marks-thread*)
        (message "On youngest Ariadne mark."))
    (message "No Adriane mark exists.")))

(defun ariadne-marks-backward ()
  "If on an ariadne point move to previous ariadne point if possible.
Else goto end-point."
  (interactive)
  (if *ariadne-marks-current*
      (if (not (equal (cons (current-buffer) (point)) (car *ariadne-marks-current*)))
          (ariadne-marks-goto-end)
        (if (cdr *ariadne-marks-current*)
            (progn
              (setf *ariadne-marks-current* (cdr *ariadne-marks-current*))
              (ariadne-marks--goto-buffer-location (car *ariadne-marks-current*))
              (message "Switched to previous ariadne-marks-mark"))
          (message "On oldest Ariadne mark.")))
    (message "No Adriane mark exists.")))

(defun ariadne-marks-unset ()
  "If on an ariadne point delete ariadne point."
  (interactive)
  (when (and *ariadne-marks-current*
             (equal (cons (current-buffer) (point)) (car *ariadne-marks-current*)))
    ;; find ariadne-marks-current in ariadne-marks-thread
    (let ((up-to-cdr (ariadne-marks--list-up-to-cdr *ariadne-marks-thread* *ariadne-marks-current*)))
      (setf *ariadne-marks-thread*  (append up-to-cdr (cdr *ariadne-marks-current*))))
    (setf *ariadne-marks-current* *ariadne-marks-thread*)
    (message "Deleted Ariadne mark")))

(defun ariadne-marks-unset-all ()
  "Remove any points from the ariadne thread."
  (interactive)
  (setq *ariadne-marks-thread* nil)
  (setq *ariadne-marks-current* *ariadne-marks-thread*)
  (message "Deleted all Ariadne marks"))
;; ariadne-marks-code ends here
;; [[file:~/p/elisp/mw/ariadne-marks/ariadne-marks.org::*Auxilliaries][ariadne-marks-auxilliaries]]

(defun ariadne-marks--return-cdr-if-in-list (list cdr)
  "Return cdr of list if LIST has that CDR in some cdr or nil."
  (when list
    (if (equal list cdr)
        list
      (ariadne-marks--return-cdr-if-in-list (cdr list) cdr))))

(defun ariadne-marks--list-up-to-cdr-acc (list cdr acc)
  (if (equal list cdr) (nreverse acc)
    (ariadne-marks--list-up-to-cdr-acc (cdr list) cdr (cons (car list) acc))))

(defun ariadne-marks--list-up-to-cdr (list cdr)
  (cl-assert (ariadne-marks--return-cdr-if-in-list list cdr))
  (ariadne-marks--list-up-to-cdr-acc list cdr nil))

(defun ariadne-marks--goto-buffer-location (buf-and-pt)
  (unless (eql (current-buffer) (car buf-and-pt))
    (switch-to-buffer (car buf-and-pt)))
  (goto-char (cdr buf-and-pt)))
;; ariadne-marks-auxilliaries ends here
;; Main\ Program:1 ends here

(provide 'ariadne-marks)

;;; ariadne-marks.el ends here
