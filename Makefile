LITERATE_SOURCE=ariadne-marks.org

ELISP_FILES=ariadne-marks.el test/ariadne-marks-tests.el

all: $(ELISP_FILES) test

compile: $(ELISP_FILES)

$(ELISP_FILES): $(LITERATE_SOURCE)
	emacs --batch --eval '(progn (find-file "ariadne-marks.org") (org-babel-tangle))'

test: $(ELISP_FILES)
	cask exec ert-runner -L .

clean:
	-rm $(ELISP_FILES)

.PHONY: all clean test
