;;; ariadne-marks-test.el --- Tests for ariadne -*- lexical-binding: t ; -*-
;; [[file:~/p/elisp/mw/ariadne-marks/ariadne-marks.org::*Test%20Program][Test\ Program:1]]
;; [[file:~/p/elisp/mw/ariadne-marks/ariadne-marks.org::*Prologue][prologue]]

;; THIS FILE HAS BEEN GENERATED.

;; Copyright (C) 2010-2017 Marco Wahl

;; Author: Marco Wahl <marcowahlsoft@gmail.com>
;; Maintainer: Marco Wahl <marcowahlsoft@gmail.com>
;; Created: 201702242151
;; Keywords: navigation
;; Homepage: http://marcowahl.github.io
;; Version: 0.0.0
;; Keywords: convenience
;; Package-Requires: ((emacs "24"))

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; prologue ends here
;; [[file:~/p/elisp/mw/ariadne-marks/ariadne-marks.org::*Commentary][ariadne-marks-test-commentary]]

;;; Commentary:
;; ariadne-marks-test-commentary ends here
;; [[file:~/p/elisp/mw/ariadne-marks/ariadne-marks.org::*Test%20Gist][ariadne-marks-test-code]]

;;; Code:

(require 'ariadne-marks)

(defvar TODO nil)


;; set and endpoint
(ert-deftest 20b98c5a44b3da5d842f2d736c89456d69231746 ()
  (should
   (with-temp-buffer
     (insert "abc")
     (ariadne-marks-set-mark)
     (let ((point (point)))
       (goto-char (point-min))
       (ariadne-marks-goto-end)
       (= (point) point)))))

(ert-deftest 8f8fe0eeee2354f2f741fc9d0b535c02705bd937 ()
  (should
   (with-temp-buffer
     (insert "abc")
     (ariadne-marks-set-mark)
     (backward-char)
     (ariadne-marks-set-mark)
     (let ((point (point)))
       (goto-char (point-min))
       (ariadne-marks-goto-end)
       (= (point) point)))))

(ert-deftest eabd55d446bcb2db978cbf5a5b3eb19e6be1e4cb ()
  (should
   (with-temp-buffer
     (insert "abc")
     (ariadne-marks-unset-all)
     (backward-char)
     (let ((point (point)))
       (ariadne-marks-goto-end)
       (= (point) point)))))

(ert-deftest fa47359ca71e7d641b7cae945fc2a26668fcf5d1 ()
  (should
   (with-temp-buffer
     (insert "abcdefg
hijkl
mnop
qrstuvwxyz")
     (ariadne-marks-unset-all)
     (backward-char)
     (ariadne-marks-set-mark)
     (search-backward "f")
     (ariadne-marks-set-mark)
     (goto-char (point-min))
     (ariadne-marks-goto-end)
     (= ?f (char-after)))))

(ert-deftest 857477e366e9d8fe2a7b784abe7395edf478982c ()
  (should
   (with-temp-buffer
     (insert "abcdefg
hijkl
mnop
qrstuvwxyz")
     (ariadne-marks-unset-all)
     (backward-char)
     (ariadne-marks-set-mark)
     (search-backward "f")
     (ariadne-marks-set-mark)
     (ariadne-marks-set-mark)
     (= 2 (length *ariadne-marks-thread*)))))


;; test ariadne-marks-go-backward
(ert-deftest 430bbb68f1b7289a33f622250596fe2123389b6a ()
  "ariadne go backward without ariadne-marks-point."
  (should
   (with-temp-buffer
     (insert "abcdefg")
     (ariadne-marks-unset-all)
     (search-backward "f")
     (cl-assert (= ?f (char-after)))
     (ariadne-marks-backward)
     (= ?f (char-after)))))

(ert-deftest 9a421ee445f2c17196b4385ee89db416883492f2 ()
  "ariadne go backward."
  (should
   (with-temp-buffer
     (insert "abcdefg")
     (ariadne-marks-unset-all)
     (search-backward "f")
     (ariadne-marks-set-mark)
     (search-backward "e")
     (ariadne-marks-backward)
     (= ?f (char-after)))))

(ert-deftest 31ad73a7b7e67b78070dec351d607e4d5e2b968b ()
  (should
   (with-temp-buffer
     (insert "abcdefg")
     (ariadne-marks-unset-all)
     (search-backward "f")
     (ariadne-marks-set-mark)
     (search-backward "e")
     (ariadne-marks-set-mark)
     (ariadne-marks-backward)
     (= ?f (char-after)))))

(ert-deftest 772b3675cd0fab6076f882161c8b68346650461a ()
  (should
   (with-temp-buffer
     (insert "abcdefg")
     (ariadne-marks-unset-all)
     (search-backward "f")
     (ariadne-marks-set-mark)
     (search-backward "e")
     (ariadne-marks-set-mark)
     (backward-char)
     (ariadne-marks-backward)
     (= ?e (char-after)))))

(ert-deftest 3191fcf939486ac42776d8cc0015f31ebe1a38f4 ()
  (should
   (with-temp-buffer
     (insert "abcdefg")
     (ariadne-marks-unset-all)
     (search-backward "f")
     (ariadne-marks-set-mark)
     (search-backward "e")
     (ariadne-marks-set-mark)
     (ariadne-marks-backward)
     (ariadne-marks-backward)
     (= ?f (char-after)))))

;; ariadne delete
(ert-deftest b9c18aec0431041daee659d2ca39ce911d5c27a0 ()
  (should
   (with-temp-buffer
     (insert "abcdefg")
     (ariadne-marks-unset-all)
     (search-backward "f")
     (ariadne-marks-set-mark)
     (ariadne-marks-unset)
     (= 0 (length *ariadne-marks-thread*)))))

(ert-deftest a25766927f7a4ef0cf8d71037490a55c75743749 ()
  (should
   (with-temp-buffer
     (insert "abcdefg")
     (ariadne-marks-unset-all)
     (search-backward "f")
     (ariadne-marks-set-mark)
     (search-backward "e")
     (ariadne-marks-set-mark)
     (ariadne-marks-unset)
     (= 1 (length *ariadne-marks-thread*)))))

(ert-deftest a4337d305d47ff826d1f3c52eb5b6fc2cb915627 ()
  (should
   (with-temp-buffer
     (insert "abcdefg")
     (ariadne-marks-unset-all)
     (search-backward "f")
     (ariadne-marks-set-mark)
     (search-backward "e")
     (ariadne-marks-set-mark)
     (ariadne-marks-backward)
     (ariadne-marks-unset)
     (ariadne-marks-backward)
     (= ?e (char-after))))
  )


;; different buffers

(ert-deftest 625de49afed4f323790f5f55e06c5e30a2a00e8f ()
    (should
     (let ((bufa (get-buffer-create "*tmp-a*"))
           (bufb (get-buffer-create "*tmp-b*")))
       (set-buffer bufa)
       (erase-buffer)
       (insert "abc")
       (search-backward "b")
       (ariadne-marks-set-mark)
       (set-buffer bufb)
       (erase-buffer)
       (insert "def")
       (search-backward "e")
       (ariadne-marks-set-mark)
       (ariadne-marks-backward)
       (= ?b (char-after))
       )))


;; auxilliaries

(ert-deftest 51a54b11251a29525e8f3217f795b6f58df5b668 ()
  (should (ariadne-marks--return-cdr-if-in-list '(a b c) '(a b c)))
  (should (ariadne-marks--return-cdr-if-in-list '(a b c) '(b c)))
  (should (ariadne-marks--return-cdr-if-in-list '(a b c) '(c)))
  (should (eq nil (ariadne-marks--return-cdr-if-in-list '(a b c) nil)))
  (should-not (ariadne-marks--return-cdr-if-in-list '(a b c) '(d c))))

(ert-deftest 2e06c59a5d59864d2435b8301eb0bc37f690cb3f ()
  (should (eq nil (ariadne-marks--list-up-to-cdr '(a b c) '(a b c))))
  (should (equal '(a) (ariadne-marks--list-up-to-cdr '(a b c) '(b c))))
  (should (equal '(a b) (ariadne-marks--list-up-to-cdr '(a b c) '(c))))
)

(ert-deftest 0b91da2ce70297776cc6725afa53328dac0c0fe7 ()
    (should
     (let ((bufa (generate-new-buffer "*test*"))
           (bufb (generate-new-buffer "*test*"))
           bufpos)
       (set-buffer bufa)
       (erase-buffer)
       (insert "abc")
       (search-backward "b")
       (setq bufpos (cons (current-buffer) (point)))
       (set-buffer bufb)
       (erase-buffer)
       (insert "def")
       (search-backward "e")
       (ariadne-marks--goto-buffer-location bufpos)
       (= ?b (char-after)))))

(ert-deftest 2337ac846ca9f34a49d5a06455efea7cdc6d1c0c ()
    (should
     (let ((bufa (generate-new-buffer "*test*"))
           bufpos)
       (set-buffer bufa)
       (erase-buffer)
       (insert "abcdef")
       (search-backward "b")
       (setq bufpos (cons (current-buffer) (point)))
       (search-forward "e")
       (ariadne-marks--goto-buffer-location bufpos)
       (= ?b (char-after)))))

;; ariadne-marks-test-code ends here
;; Test\ Program:1 ends here
(provide 'ariadne-marks-test)

;;; ariadne-marks-test.el ends here
